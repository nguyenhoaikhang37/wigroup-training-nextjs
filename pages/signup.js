import Head from 'next/head';
import SignUp from '../components/signup';

const Register = () => {
	return (
		<>
			<Head>
				<title>Register User</title>
			</Head>
			<SignUp />
		</>
	);
};

export default Register;
